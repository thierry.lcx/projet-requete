from flask import Flask, request, render_template

app = Flask(__name__)

noms=[]
types=[]
generations=[]


def datainit():
    '''
    initialise la liste de noms
    '''
    global noms, types, generations
    noms=["Salamèche","Carapuce","Bulbizarre"]
    types=["Feu","Eau","Plante"]
    generations=[1,1,1]

@app.route('/index.html')
def index():
    return render_template('index.html')

@app.route('/table.html', methods = ["GET"])
def tableau():
    return render_template('table.html',noms=noms,types=types,generations=generations, len=len(noms))

@app.route('/reinit.html', methods = ["GET"])
def reinit():
    datainit()
    return render_template('table.html',noms=noms,types=types,generations=generations, len=len(noms))

@app.route('/delrow.html', methods = ["GET"])
def delrow():
    pos = int(request.args.get('pos', 0))
    #TODO: comment supprimer la ligne ?
    noms.pop(pos)
    types.pop(pos)
    generations.pop(pos)
    return render_template('table.html',noms=noms,types=types,generations=generations, len=len(noms))

@app.route('/ajout.html', methods=["GET"])
def ajout():
    return render_template('ajout.html')

@app.route('/doc.html', methods=["GET"])
def doc():
    return render_template('doc.html')

@app.route('/form_traite.html', methods=["POST"])
def traitement_formulaire():
    nom = request.form['nom']
    noms.append(nom)
    tipe = request.form['type']
    types.append(tipe) 
    gen = request.form['generation']
    generations.append(gen)
    return tableau()


datainit()
app.run(host='0.0.0.0', port='5000', debug=True)            